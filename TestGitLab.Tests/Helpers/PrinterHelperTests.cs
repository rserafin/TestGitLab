﻿using NUnit.Framework;
using TestGitLab.Helpers;

namespace TestGitLab.Tests.Helpers
{
    [TestFixture]
    public class PrinterHelperTests
    {
        [Test]
        public void ConcatNameAndSurnameShouldReturnFullName()
        {
            //Arrange
            string Name = "Jane";
            string Surname = "Test";


            //Act
            var sut = new PrinterHelper();
            var result = sut.ConcatNameAndSurname(Name, Surname);

            //Assert
            var expectedFullName = "Jane Test";
            Assert.AreEqual(expectedFullName, result);
        }
    }
}
