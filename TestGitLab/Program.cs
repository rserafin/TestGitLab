﻿using System;
using TestGitLab.Helpers;

namespace TestGitLab
{
    class Program
    {
        static void Main(string[] args)
        {
            string Name = "Rafal";
            string Surname = "Serafin";

            PrinterHelper printerHelper = new PrinterHelper();
            string fullName = printerHelper.ConcatNameAndSurname(Name, Surname);
            Console.WriteLine(fullName);
            Console.ReadLine();
        }
    }
}
