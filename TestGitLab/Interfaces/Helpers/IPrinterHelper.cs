﻿namespace TestGitLab.Interfaces.Helpers
{
    interface IPrinterHelper
    {
        string ConcatNameAndSurname(string name, string surname);
    }
}
