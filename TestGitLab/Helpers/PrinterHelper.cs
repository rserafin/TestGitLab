﻿using System;
using TestGitLab.Interfaces.Helpers;

namespace TestGitLab.Helpers
{
    public class PrinterHelper : IPrinterHelper
    {
        public string ConcatNameAndSurname(string name, string surname)
        {
            return String.Format("{0} {1}", name.Trim(), surname.Trim());
        }
    }
}
